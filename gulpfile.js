var gulp = require('gulp'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    jshint = require('gulp-jshint'),
    stylish = require('jshint-stylish'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    browserSync = require('browser-sync').create()

// Definició dels directoris d'origen "srcPaths".
var srcPaths = {
    data: 'src/data/',
    files: 'src/',
    images: 'src/img/',
    libs: 'src/vendor/',
    scripts: 'src/js/',
    styles: 'src/scss/'
};

// Definició dels directoris de destí "distPaths".
var distPaths = {
    data: 'dist/data/',
    files: 'dist/',
    images: 'dist/img/',
    libs: 'dist/vendor/',
    scripts: 'dist/js/',
    styles: 'dist/css/'
};

// Tasca "clean" que neteja del directori dist/ i els seus diferents subdirectoris.
gulp.task('clean', function (cb) {
    return del([
        distPaths.data + '*.json',
        distPaths.files + '*.html',
        distPaths.images + '**/*',
        distPaths.libs + '**/*',
        distPaths.scripts + '**/*',
        distPaths.styles + '*.css'
        ,], cb);
});

// Tasca "html" que copia l'ultim fitxers modificats html.
gulp.task('html', function () {
    return gulp.src([srcPaths.files + '*.html'])
        .pipe(gulp.dest(distPaths.files))
        .pipe(browserSync.stream());
});

// Tasca "vendor" que copia les ultimes llibreries afegides.
gulp.task('vendor', function () {
    return gulp.src([srcPaths.libs + '**/*'])
        .pipe(gulp.dest(distPaths.libs))
        .pipe(browserSync.stream());
});

// Tasca "data" que copia la ultima versió de les dades inicials de la aplicació.
gulp.task('data', function () {
    return gulp.src([srcPaths.data + '**/*'])
        .pipe(gulp.dest(distPaths.data))
        .pipe(browserSync.stream());
});

/*
* Tasca "copy" que copia els ultims fitxers modificats que no requereixen de cap processat extra.
* Aquesta crida les tasques: html, vendor i data.
*/
gulp.task('copy', ['html', 'vendor', 'data'], function () {});

// Tasca "imagemin" que comprimeix/optimitza i copia les imatges.
gulp.task('imagemin', function () {
    return gulp.src([srcPaths.images + '*.*'])
        .pipe(imagemin({
            progressive: true,
            interlaced: true,
            svgoPlugins: [{removeUnknownsAndDefaults: false}, {cleanupIDs: false}]
        }))
        .pipe(gulp.dest(distPaths.images))
        .pipe(browserSync.stream());
});

// Tasca "css" que processa (transpila) els fitxers scss a css, generant els corresponents sourcemaps.
gulp.task('css', function () {
    return gulp.src([srcPaths.styles + '*.scss'])
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest(distPaths.styles))
        .pipe(browserSync.stream());
});

// Tasca "lint" que processa fitxers JS amb JSHint tot buscant-hi errors.
gulp.task('lint', function () {
    return gulp.src([srcPaths.scripts + '*.js'])
        .pipe(jshint())
        .pipe(jshint.reporter(stylish));
});

// Tasca "js" que en un primer moment executa la tasca "lint" per després processar i concadenar els fitxers JS tot generant un únic fitxar minificat amb els sourcemaps en una carpeta independent (en comptes de en el mateix fitxer).
gulp.task('js', ['lint'], function () {
    return gulp.src([srcPaths.scripts + 'main.js', srcPaths.scripts + 'contact.js', srcPaths.scripts + 'contactlist.js'])
        .pipe(sourcemaps.init())
        .pipe(concat('all.min.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest(distPaths.scripts))
        .pipe(browserSync.stream());
});

/*
* Tasca "serve" que realitza un buil complet de la aplicació (a través de les tasques: copy, imagemin, css i js) per posteriorment mostrar el resultat en diferents navegadors web amb BrowserSync.
* Ademés es monitoritzaran els canvis realitzats en els fitxers sensibles per el projecte (*.html, *.js o *.scss).
*/
gulp.task('serve', ['copy', 'imagemin', 'css', 'js'], function () {
    browserSync.init({
        logLevel: "info",
        browser: ["chromium-browser", "firefox"],
        server: "./dist/"
    });

    gulp.watch(srcPaths.data + '*.json', ['data']);
    gulp.watch(srcPaths.files + '*.html', ['html']);
    gulp.watch(srcPaths.images + '**/*', ['imagemin']);
    gulp.watch(srcPaths.scripts + '**/*.js', ['js']);
    gulp.watch(srcPaths.styles + '**/*.scss', ['css']);

});

/*
* Tasca per defecte de gulp.
* Primer execuratà la tasca clean, i un cop finalitzada, llançarà la tasca serve.
*/
gulp.task('default', ['clean'], function (){
    gulp.start('serve');
});